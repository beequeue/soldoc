
angular.module( 'soldoc.json', [
  'ui.router'
])

/**
 * Each section or module of the site can also have its own routes. AngularJS
 * will handle ensuring they are all available at run-time, but splitting it
 * this way makes each module more "self-contained".
 */
.config(function config( $stateProvider ) {
  $stateProvider.state( 'json', {
    url: '/json',
    views: {
      "main": {
        controller: 'JsonCtrl',
        templateUrl: 'json/json.tpl.html'
      }
    },
    data:{ pageTitle: 'Edit JSON' }
  });
})

/**
 * And of course we define a controller for our route.
 */
.controller( 'JsonCtrl', function JsonController( $scope ) {
})

;

